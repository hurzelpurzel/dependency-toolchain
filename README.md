# Dependency Toolchain


Target of this project is to create a Toolchain to experiment with SBOMs on Kubernetes

1.) host dependeny-track tool
2.) create a Tekton task with syft to create SBOMs from images an publish them to dependency-track
3.) Use Skoepo Task to get images from repo. e.g https://hub.docker.com/u/vulnerables



## To start pipeline use
k create -f  pipelinerun.yaml

## Howto create API Key
https://docs.dependencytrack.org/integrations/rest-api/
The new team / api-key must have the BOM_UPLOAD and PROJECT_CREATION_UPLOAD permission

## Doing BOM upload
https://docs.dependencytrack.org/usage/cicd/

Test BOMS @ https://github.com/CycloneDX/bom-examples