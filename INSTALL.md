# dep-track

helm upgrade -i dep-track helm/dependency-track -f helm/k3s-dt-values.yaml

# Tekton
see: https://tekton.dev/docs/installation/pipelines/


kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml

## Tekton dashboard
kubectl apply --filename https://storage.googleapis.com/tekton-releases/dashboard/latest/release.yaml


## Tekton triggers

Follow
https://tekton.dev/docs/getting-started/triggers/
